const i18n = require('i18n')

module.exports = {
  configure: function (locale) {
    i18n.configure({
      locales: ['es', 'en'],
      directory: `${__dirname}/locales`
    })
    i18n.setLocale(locale || 'es')
  },
  i18n
}
